package com.concesionaria.clientes.api.customer;

import com.concesionaria.clientes.api.baseEntity.*;
import com.concesionaria.clientes.api.utils.Status;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity(name = "customer")
public class Customer extends BaseEntityId {

    @Column(name="name")
    private String name;

    @Column(name="last_name")
    private String lastName;

    @Column(name="username")
    private String username;

    @Column(name="password")
    private String password;

    @Column(name="email")
    private String email;

    @Column(name="address")
    private String address;

    @Column(name="status")
    private Status status;

    public Customer() {
        super();
    }
}
