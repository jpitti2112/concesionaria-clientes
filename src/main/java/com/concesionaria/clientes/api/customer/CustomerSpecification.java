package com.concesionaria.clientes.api.customer;

import com.concesionaria.clientes.api.utils.Status;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class CustomerSpecification implements Specification<Customer> {

    private Customer filter;


    public CustomerSpecification(Customer filter) {
        super();
        this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<Customer> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Predicate p = criteriaBuilder.conjunction();

        if (filter.getId() != null) {

            p.getExpressions().add(criteriaBuilder.equal(root.get("id"), filter.getId()));

        }

        if (filter.getName() != null && !filter.getName().isEmpty()) {

            p.getExpressions().add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), filter.getName().toLowerCase() + "%"));

        }

        if (filter.getLastName() != null && !filter.getLastName().isEmpty()) {

            p.getExpressions().add(criteriaBuilder.like(criteriaBuilder.lower(root.get("lastName")), filter.getLastName().toLowerCase() + "%"));

        }

        if (filter.getUsername() != null && !filter.getUsername().isEmpty()) {

            p.getExpressions().add(criteriaBuilder.like(criteriaBuilder.lower(root.get("username")), filter.getUsername().toLowerCase() + "%"));

        }

        if (filter.getStatus() != null) {

            if(!filter.getStatus().equals(Status.ALL)){

                p.getExpressions().add(criteriaBuilder.equal(root.get("status"), filter.getStatus()));

            }

        }

        return p;
    }
}
