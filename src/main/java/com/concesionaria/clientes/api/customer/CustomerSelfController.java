package com.concesionaria.clientes.api.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static endpoint.EndPoints.CUSTOMER;
import static endpoint.EndPoints.FIND;

@RestController()
@RequestMapping(CUSTOMER)
public class CustomerSelfController {

    @Autowired
    private CustomerService customerService;

    @GetMapping()
    public Page<Customer> findall(Pageable pageable) {

        return customerService.findAll(pageable);
    }

    @GetMapping("/{customerId}")
    public Customer showOne(@PathVariable("customerId") Long customerId) {

        return customerService.findById(customerId);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Customer create(@Valid @RequestBody Customer customer) {
        return customerService.create(customer);
    }

    @PutMapping("/{customerId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Customer edit(@Valid @RequestBody Customer customer) {
        return customerService.update(customer);
    }

    @RequestMapping(value = "/{customerId}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("customerId") Long customerId) {

        customerService.delete(customerId);
    }

    @PostMapping(FIND + "/filter")
    public Page<Customer> findByFilter(@RequestBody Customer customer, @PageableDefault(size = 10) Pageable pageable) {

        return customerService.findByFilter(customer, pageable);

    }


}
