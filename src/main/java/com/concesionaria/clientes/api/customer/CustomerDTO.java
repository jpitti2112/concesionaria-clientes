package com.concesionaria.clientes.api.customer;

import com.concesionaria.clientes.api.utils.Status;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerDTO {

    private Long id;

    private String name;

    private String lastName;

    private String username;

    private String password;

    private String email;

    private String address;

    private Status status;

}
