package com.concesionaria.clientes.api.customer;

import com.concesionaria.clientes.api.baseInterfases.EntityPageService;
import com.concesionaria.clientes.api.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService implements EntityPageService<Customer> {

    @Autowired
    private CustomerDao dao;

    @Override
    public List<Customer> findAll() {
        return null;
    }

    @Override
    public Page<Customer> findAll(Pageable pageable) {
        return dao.findAll(pageable);
    }

    @Override
    public List<Customer> findByActive(Boolean active) {
        return null;
    }

    @Override
    public Page<Customer> findByActive(Boolean active, Pageable pageable) {
        return null;
    }

    @Override
    public Customer findById(Long id) {
        return dao.findById(id).get();
    }

    @Override
    public Customer create(Customer entity) {
        entity.setStatus(Status.ENABLED);

        return dao.save(entity);
    }

    @Override
    public Customer update(Customer entity) {
        return dao.save(entity);
    }

    @Override
    public void create(List<Customer> entity) {

    }

    @Override
    public void update(List<Customer> entity) {

    }

    @Override
    public void delete(Customer entity) {

    }

    @Override
    public void delete(Long customerId) {
        Customer policy = findById(customerId);

        policy.setStatus(Status.DISABLED);

        update(policy);
    }

    public Page<Customer> findByFilter(Customer customer, Pageable pageable) {
        CustomerSpecification customerSpecification = new CustomerSpecification(customer);

        return dao.findAll(customerSpecification, pageable);
    }
}
