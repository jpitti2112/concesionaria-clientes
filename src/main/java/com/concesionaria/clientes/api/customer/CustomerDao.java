package com.concesionaria.clientes.api.customer;

import com.concesionaria.clientes.api.utils.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CustomerDao extends PagingAndSortingRepository<Customer, Long>, JpaSpecificationExecutor<Customer> {

}
