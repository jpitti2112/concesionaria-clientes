package com.concesionaria.clientes.api.utils;

import endpoint.Constantes;

public enum Status {

    ENABLED(Constantes.ENABLED){

        @Override
        public Status enabled() {
            return ENABLED;
        }

        @Override
        public Status disabled() {
            return DISABLED;
        }
    },
    DISABLED(Constantes.DISABLED){

        @Override
        public Status enabled() {
            return ENABLED;
        }

        @Override
        public Status disabled() {
            return DISABLED;
        }
    },
    ALL(Constantes.ALL){

        @Override
        public Status enabled() {
            return ENABLED;
        }

        @Override
        public Status disabled() {
            return DISABLED;
        }

    };


    public abstract Status enabled();
    public abstract Status disabled();

    private final String value;

    Status(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }
}
