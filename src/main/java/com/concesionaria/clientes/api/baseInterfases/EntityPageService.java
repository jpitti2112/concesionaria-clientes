package com.concesionaria.clientes.api.baseInterfases;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface EntityPageService<T> {

    List<T> findAll();

    Page<T> findAll(Pageable pageable);

    List<T> findByActive(Boolean active);

    Page<T> findByActive(Boolean active, Pageable pageable);

    T findById(Long id);

    T create(T entity);

    T update(T entity);

    void create(List<T> entity);

    void update(List<T> entity);

    void delete(T entity);

    void delete(Long id);
}
