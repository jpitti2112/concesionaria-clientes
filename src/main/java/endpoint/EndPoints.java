package endpoint;

public class EndPoints {

    public static final String ROOT = "/";
    public static final String CREATE = "/create";
    public static final String ADD = "/add";

    public static final String CUSTOMER = "/customer";
    public static final String FIND = "/findBy";

}
