# Prueba Consultec - Microservicio de Clientes #
contacto: José Pittí - josepitti2112 [at] gmail.com

###Objetivo###
La empresa de Automóviles XXXXX, desea tener un sistema de información que le permita llevar el control de sus clientes. Actualmente la empresa posee una red de
concesionarios a nivel nacional, alrededor de 15. Pero requiere tener un sistema que le
permite gestionar la información de sus clientes y segmentar el mismo por
concesionario.

### Herramientas ###

* IntelliJ IDEA
* Git +2.13
* H2 Database
* Lombok
* Apache Maven 3.6.0
* SpringBoot 2.2.1.RELEASE
* Postman 7.12.0

### Propiedades del Microservico ###
* port: 8080
* name: customer
* rootUrl: /customer

### JSON CRUD ###

##### CREATE #####
* url: POST http://localhost:8080/customer
* Content-Type: application/json

{
	"name": "Carlos",
	"lastName": "Vargas",
	"username": "cvargas",
	"password": "123456",
	"email": "cvargas@consultec.com",
	"address": "Ciudad de Panamá",
	"createdBy": "admin"
}

##### EDIT #####
* url: PUT http://localhost:8080/customer/1
* Content-Type: application/json

{
	"id": 1,
	"name": "Pedro",
	"lastName": "Vargas",
	"username": "pvargas",
	"password": "123456",
	"email": "pvargas@consultec.com",
	"address": "Ciudad de Panamá",
	"createdBy": "admin"
}

##### DELETE #####
* url: DELETE http://localhost:8080/customer/1
* Content-Type: application/json

{
	"id": 1
}

##### FIND BY ID #####
* url: GET http://localhost:8080/customer/1

##### FIND ALL #####
* url: GET http://localhost:8080/customer

##### FIND BY FILTER #####
* url: POST http://localhost:8080/customer/findBy/filter
* Content-Type: application/json

{
	"name": "Pedro",
	"lastName": "Vargas",
	"username": "pvargas",
	"status": "ENABLED"
}

